package com.simone.limpida.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.simone.limpida.R;

public class RegisterActivity extends AppCompatActivity {

    private String[] countryCodes = {"+33","+34","+23"};
    private Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);


        spinner = findViewById(R.id.countryCodeSpinner);
        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, countryCodes);
        spinner.setAdapter(adapter);


    }

    // test click on continue
    public void clickOnContinue(View view)
    {
        startActivity(new Intent(this , SignUpActivity.class));
    }
}