package com.simone.limpida.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.simone.limpida.Activities.Adapters.AdatperForImagesRecycler;
import com.simone.limpida.R;

import java.util.ArrayList;
import java.util.List;

public class NuovaScansioneActivity extends AppCompatActivity {


    private RecyclerView recyclerView;
    private static List<Bitmap> imageUri = new ArrayList<>();
    private static TextView imageCounter;
    private AdatperForImagesRecycler adatper;
    private static int sizeOfArray;
    private boolean activeLoc = true;
    private Toolbar mToolbar;
    private ImageView locationIcon;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nuova_scansione);

        mToolbar = findViewById(R.id.nuovaScanToolbar);
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        locationIcon  = findViewById(R.id.locationIcon);

        imageCounter = findViewById(R.id.imageCounter);
        recyclerView = findViewById(R.id.imagesRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));


    }


    // take image from camera
    public void selectImage(View view) {

        if (sizeOfArray < 3) {

            if (checkPermision()) {
                Intent takeImage = new Intent();
                takeImage.setAction(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(takeImage, 10);
            } else {
                requestPermission();
            }

        } else {
            Toast.makeText(this, "Already select 3 images", Toast.LENGTH_SHORT).show();
        }


    }


    private boolean checkPermision() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {


            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }

        }

        return true;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 22);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 22 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, "Permission Granted", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Please enable permission", Toast.LENGTH_SHORT).show();
            requestPermission();
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 10 && resultCode == RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");

            imageUri.add(photo);
            Log.d("bitmapImage", "OnResult: " + photo);

            adatper = new AdatperForImagesRecycler(imageUri);
            recyclerView.setAdapter(adatper);
            imageCounter.setText(imageUri.size() + "/3");
            sizeOfArray = imageUri.size();


        } else {
            Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show();
        }
    }


    public static void updateText(int size) {
        sizeOfArray = size;
        imageCounter.setText(size + "/3");
    }


    public void submitBtnClick(View view) {
        getListSize(sizeOfArray);
    }


    public void getListSize(int size) {
        if (size == 0 ) {
            showStatusDialog("fail");
        } else if (size > 0 && size <= 3) {
            showStatusDialog("success");
        }
    }

    @SuppressLint("ResourceType")
    private void showStatusDialog(String status) {
        View customView;
        final Dialog dialog;
        AlertDialog.Builder alert;

        if (status == "success") {
            customView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.custom_success_dialog, null);
            alert = new AlertDialog.Builder(NuovaScansioneActivity.this);
            alert.setView(customView);
            dialog = alert.create();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            Button closeBtn = customView.findViewById(R.id.closeBtnInDialog);

            closeBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    dialog.dismiss();
                    startActivity(new Intent(NuovaScansioneActivity.this, HomeActivity.class));
                    NuovaScansioneActivity.this.finish();

                }
            });


        } else {
            customView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.custom_failure_dialog, null);

            alert = new AlertDialog.Builder(NuovaScansioneActivity.this);
            alert.setView(customView);
            dialog = alert.create();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

            Button closeBtn = customView.findViewById(R.id.closeBtnInDialog);

            closeBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    dialog.dismiss();

                }
            });


        }


        dialog.show();
    }

    // test click on location icon
    public void locTestClick(View view)
    {
        if(activeLoc)
        {
            locationIcon.setImageResource(R.drawable.loc_green);
            activeLoc = false;
        }
        else
        {
            locationIcon.setImageResource(R.drawable.ic_baseline_location_on_24);
            activeLoc = true;
        }

    }


}