package com.simone.limpida.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.simone.limpida.Activities.Adapters.AdapterForRecyclerView;
import com.simone.limpida.R;

public class AttivitaActivity extends AppCompatActivity {

    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_attivita);


        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        AdapterForRecyclerView adapter = new AdapterForRecyclerView(this);

        recyclerView.setAdapter(adapter);



    }

    // show dialog when click on graph
    public void showCongrulationDialog(View view)
    {

          showNotificationDialog();

    }




    @SuppressLint("ResourceType")
    private void showNotificationDialog() {
        View customView;
        final Dialog dialog;
        AlertDialog.Builder alert;
        customView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.custom_congrats_dialog, null);
        alert = new AlertDialog.Builder(AttivitaActivity.this);
        alert.setView(customView);
        dialog = alert.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        Button btnContinua = customView.findViewById(R.id.continuaBtn);

        btnContinua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(AttivitaActivity.this, HomeActivity.class));
                dialog.dismiss();

            }
        });


        dialog.show();
    }


}