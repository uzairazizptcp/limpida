package com.simone.limpida.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.simone.limpida.R;

public class MainActivity extends AppCompatActivity {

    private int[] images = {R.drawable.first, R.drawable.second, R.drawable.third};
    private String[] names = {"First", "Second", "Third"};
    private int counter = 0;
    private ImageView displayImage;;
    private TextView title;
    TextView[] dot;
    LinearLayout layout_dot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);

        layout_dot = (LinearLayout) findViewById(R.id.dotsLayout);
        dot = new TextView[names.length];

        displayImage = findViewById(R.id.sliderImage);
        title = findViewById(R.id.imageTitle);

        setAllDotsColor();

        displayImage.setImageResource(images[counter]);
        title.setText(names[counter]);
        addDot(counter);

    }

    // click on image view to change image and text
    public void onImageClick(View view)
    {
        if(counter < images.length)
        {
            displayImage.setImageResource(images[counter]);
            title.setText(names[counter]);

            addDot(counter);

            counter++;
        }
        else
        {
            startActivity(new Intent(this, LoginActivity.class));

        }

    }



    public void addDot(int page_position) {
        //set active dot color
        dot[page_position].setTextColor(getResources().getColor(R.color.colorAccent));
    }

    private void setAllDotsColor()
    {
        layout_dot.removeAllViews();

        for (int i = 0; i < dot.length; i++) {
            dot[i] = new TextView(this);
            dot[i].setText(Html.fromHtml("&#9679;"));
            dot[i].setTextSize(10);
            //set default dot color
            dot[i].setTextColor(getResources().getColor(R.color.colorPrimaryDark));
            // layout_dot.addView(dot[i]);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);

            params.setMargins(6, 0, 6, 0);

            layout_dot.addView(dot[i], params);

        }
    }

}