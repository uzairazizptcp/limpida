package com.simone.limpida.Activities.Adapters;

import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.simone.limpida.Activities.NuovaScansioneActivity;
import com.simone.limpida.R;

import java.util.List;

public class AdatperForImagesRecycler extends RecyclerView.Adapter<AdatperForImagesRecycler.MyViewHolder>
{
    private List<Bitmap> imageUriList;


    public AdatperForImagesRecycler(List<Bitmap> imageUriList) {
        this.imageUriList = imageUriList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View myView = LayoutInflater.from(parent.getContext()).inflate(R.layout.desgin_for_image_recycler, null);
        return new MyViewHolder(myView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position)
    {
        Bitmap bitmap = imageUriList.get(position);

        Log.d("bitmapImage", "onBindViewHolder: "+bitmap);

        holder.imageView.setImageBitmap(bitmap);

        holder.closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                imageUriList.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position, imageUriList.size());
                NuovaScansioneActivity.updateText(imageUriList.size());

            }
        });

    }

    @Override
    public int getItemCount() {
        return imageUriList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView imageView, closeBtn;



        public MyViewHolder(@NonNull View itemView)
        {
            super(itemView);
            imageView = itemView.findViewById(R.id.selectImage);
            closeBtn = itemView.findViewById(R.id.closeImage);


        }
    }
}
