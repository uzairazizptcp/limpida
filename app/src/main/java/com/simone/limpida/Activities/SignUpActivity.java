package com.simone.limpida.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.simone.limpida.R;

public class SignUpActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
    }

    public void testClick(View view)
    {
        startActivity(new Intent(this, HomeActivity.class));
    }

    public void forgotTestClick(View view)
    {
        startActivity(new Intent(this, ForgotPasswordActivity.class));

    }
}