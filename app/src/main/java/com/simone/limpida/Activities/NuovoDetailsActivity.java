package com.simone.limpida.Activities;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.simone.limpida.R;

public class NuovoDetailsActivity extends AppCompatActivity  {

    private ImageView backBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_nuovo_details);

        backBtn = findViewById(R.id.back_imae_btn);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(NuovoDetailsActivity.this , NuovoActivity.class));
                NuovoDetailsActivity.this.finish();
            }
        });




    }



    // test click to move next activity
    public void testClick(View view) {
      // startActivity(new Intent(this , HomeActivity.class));

    }


}