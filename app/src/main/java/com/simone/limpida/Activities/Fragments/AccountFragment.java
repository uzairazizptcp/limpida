package com.simone.limpida.Activities.Fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.simone.limpida.Activities.AttivitaActivity;
import com.simone.limpida.Activities.LoginActivity;
import com.simone.limpida.Activities.NuovaScansioneActivity;
import com.simone.limpida.R;

public class AccountFragment extends Fragment {

    private View myView;
    private RelativeLayout exit, fourth , cronologia;

    public AccountFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
         myView = inflater.inflate(R.layout.fragment_account, container, false);

         exit = myView.findViewById(R.id.sixthItem);
         fourth = myView.findViewById(R.id.fourthItem);
         cronologia = myView.findViewById(R.id.firstItem);


         cronologia.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 startActivity(new Intent(getActivity() , AttivitaActivity.class));
                 getActivity().finish();
             }
         });

         fourth.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 startActivity(new Intent(getActivity() , NuovaScansioneActivity.class));
                 getActivity().finish();
             }
         });


         exit.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {

                 startActivity(new Intent(getActivity(), LoginActivity.class));
                 getActivity().finish();

             }
         });


         return  myView;
    }
}