package com.simone.limpida.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.simone.limpida.R;

public class LoginActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    // test click move to register activity
    public void createAccountClick(View view)
    {
        startActivity(new Intent(this, RegisterActivity.class));
    }

    // login button click
    public void loginBtnClick(View view) {
        startActivity(new Intent(this, HomeActivity.class));

    }
}