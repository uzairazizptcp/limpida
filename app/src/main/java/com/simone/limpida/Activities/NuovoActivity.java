package com.simone.limpida.Activities;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.simone.limpida.R;

public class NuovoActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private ImageView favImage;
    private boolean react = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nuovo);

        mToolbar = findViewById(R.id.toolbarNuovo);
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        favImage = findViewById(R.id.loveReactImage);


        favImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(react)
                {
                    favImage.setImageResource(R.drawable.fav_red);
                    react = false;
                }
                else
                {
                    favImage.setImageResource(R.drawable.ic_baseline_favorite_border_24);
                    react = true;
                }

            }
        });


    }

    // click move to next activity
    public void testClick(View view) {

        startActivity(new Intent(this , NuovoDetailsActivity.class));
    }
}