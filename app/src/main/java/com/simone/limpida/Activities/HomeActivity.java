package com.simone.limpida.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.simone.limpida.Activities.Fragments.AccountFragment;
import com.simone.limpida.Activities.Fragments.HomeFragment;
import com.simone.limpida.Activities.Fragments.MapFragment;
import com.simone.limpida.Activities.Fragments.SnapshotFragment;
import com.simone.limpida.R;

public class HomeActivity extends AppCompatActivity {


    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        textView  = findViewById(R.id.titleToolbar);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, new HomeFragment());
        transaction.commit();


        BottomNavigationView nf = (BottomNavigationView) findViewById(R.id.bottomNav);
        nf.setSelectedItemId(R.id.home);

        nf.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                if (menuItem.getItemId() == R.id.home) {
                    textView.setText("Home");
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.container, new HomeFragment());
                    transaction.commit();
                    return true;
                }
                if (menuItem.getItemId() == R.id.account) {
                    textView.setText("Account");
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.container, new AccountFragment());
                    transaction.commit();

                    return true;
                }

                if (menuItem.getItemId() == R.id.flash) {
                    textView.setText("Snapshot");
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.container, new SnapshotFragment());
                    transaction.commit();
                    return true;
                }

                if (menuItem.getItemId() == R.id.explore) {
                    textView.setText("Explore");
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.container, new MapFragment());
                    transaction.commit();
                    return true;
                }

                return false;
            }
        });


    }


    // click to add items activity
    public void testClickToAddItem(View view) {
        startActivity(new Intent(this, NuovaScansioneActivity.class));

    }

    // click to show notification dialog
    public void showNotificationDialog(View view)
    {
        showNotificationDialog();
    }


    @SuppressLint("ResourceType")
    private void showNotificationDialog() {
        View customView;
        final Dialog dialog;
        AlertDialog.Builder alert;
        customView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.custom_notificaiton_dialog, null);
        alert = new AlertDialog.Builder(HomeActivity.this);
        alert.setView(customView);
        dialog = alert.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.TOP | Gravity.LEFT;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);


        RelativeLayout firstItem = customView.findViewById(R.id.firstItem);
        RelativeLayout secondItem = customView.findViewById(R.id.secondItem);

        firstItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(HomeActivity.this, NuovoActivity.class));
                dialog.dismiss();

            }
        });

        secondItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivity.this, AttivitaActivity.class ));
                dialog.dismiss();
            }
        });


        dialog.show();
    }
}