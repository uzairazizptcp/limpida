package com.simone.limpida.Activities.Fragments;

import android.content.Intent;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.simone.limpida.Activities.HomeActivity;
import com.simone.limpida.R;

public class BottomSheet extends BottomSheetDialogFragment {


   private  View myView;


    public BottomSheet() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        myView = inflater.inflate(R.layout.fragment_bottom_sheet, container, false);

        Button button = myView.findViewById(R.id.imposta);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getContext(), HomeActivity.class));
                getActivity().finish();

            }
        });

        return myView;

    }


}