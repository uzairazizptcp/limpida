package com.simone.limpida.Activities.Fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.simone.limpida.Activities.AttivitaActivity;
import com.simone.limpida.Activities.NuovoActivity;
import com.simone.limpida.R;

public class HomeFragment extends Fragment {


    private View homeView;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        homeView = inflater.inflate(R.layout.fragment_home, container, false);

        ImageView firstCard = homeView.findViewById(R.id.firstCard);
        ImageView secondCard = homeView.findViewById(R.id.secondCard);
        ImageView thirdCard = homeView.findViewById(R.id.thirdCard);
        ImageView fourthCard = homeView.findViewById(R.id.fourthCard);



        firstCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getContext(), NuovoActivity.class));
            }
        });

        secondCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), AttivitaActivity.class));


            }
        });

        thirdCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new BottomSheet().show(getActivity().getSupportFragmentManager(), "Bottom Sheet");


            }
        });

        fourthCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Fragment fragment = new SnapshotFragment();
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.homeFragmentContainer, fragment);
                transaction.commit();

            }
        });






        return homeView;
    }
}